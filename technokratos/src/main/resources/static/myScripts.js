function saveCV(fullName,age,phoneNumber,email, bio){
    let newCV = {
        "fullName":fullName,
        "age":age,
        "phoneNumber":phoneNumber,
        "email":email,
        "bio":bio,
    }
    $.ajax({
        type:"POST",
        url:"/saveCV",
        data: JSON.stringify(newCV),
        success:()=>{writeTable()
            console.log(newCV)},
        statusCode: {
            500: function() {
                console.log( "NOT OK" );
            }
        },
        contentType:"application/json",

    })
}
function clearInput(){
    document.getElementById("fullNameInput").value = "";
    document.getElementById("ageInput").value = "";
    document.getElementById("emailInput").value = "";
    document.getElementById("phoneInput").value = "";
    document.getElementById("bioInput").value = "";
}
function del(id){
        $.ajax({
            type:"POST",
            url:"/del/"+id,
            success:()=>{
                writeTable()
            },
        })
}
function writeTable() {
    $.ajax({
        type: "GET",
        url: "/getCV",
        dataType: "json",
        success: function(data) {
            $('#myTable tbody > tr').remove();
            for (var i=0; i<data.length; i++) {
                var row = $('<tr>' +
                    '<td>' + data[i].id + '</td>' +
                    '<td>' + data[i].fullName + '</td>' +
                    '<td>' + data[i].age + '</td>' +
                    '<td>'  + data[i].phoneNumber + '</td>' +
                    '<td>'  + data[i].email + '</td>' +
                    '<td>'  + data[i].bio + '</td>' +
                    `<td><div class="btn-group"><button type="button" class="btn btn-outline-primary mb-3" onclick="del('${data[i].id}')">Удалить</button><button type="button" class="btn btn-secondary mb-3" onclick="refactor('${data[i].id}')">Редактировать</button></div>
                         </td>` + '</tr>');
                $('#tdata').append(row);
            }
        }
    })
}
function refbtnvisibility(){
    document.getElementById("refactorSave").style.visibility = "hidden"
}
function refactor(id){
    $.ajax({
        type:"GET",
        url:"/get/"+id,
        dataType: "json",
        success: function (data){
            document.getElementById("fullNameInput").value = data.fullName;
            document.getElementById("ageInput").value = data.age;
            document.getElementById("emailInput").value = data.email;
            document.getElementById("phoneInput").value = data.phoneNumber;
            document.getElementById("bioInput").value = data.bio;
            document.getElementById("hiddenID").value = data.id;
            document.getElementById("refactorSave").style.visibility = "visible"
        },
        error:console.log("Some problem here"),

    })
}
function refactorSave(id, fullName,age,phoneNumber,email, bio,){
    let newCV = {
        "id":id,
        "fullName":fullName,
        "age":age,
        "phoneNumber":phoneNumber,
        "email":email,
        "bio":bio,

    }
    $.ajax({
        type:"POST",
        url:"/refactorSave",
        data: JSON.stringify(newCV),
        success:()=>{
            writeTable()
            console.log(newCV)
            refbtnvisibility()},
        statusCode: {
            500: function() {
                console.log( "NOT OK" );
            }
        },
        contentType:"application/json",

    })
}
function myFunction() {

    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
