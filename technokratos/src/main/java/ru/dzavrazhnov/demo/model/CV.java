package ru.dzavrazhnov.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "CV")
public class CV {
    @Id
    private String id;
    private String fullName;
    private int age;
    private String email;
    private String phoneNumber;
    private String bio;

}
