package ru.dzavrazhnov.demo.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CVdto {
    private String fullName;
    private int age;
    private String email;
    private String phoneNumber;
    private String bio;
}
