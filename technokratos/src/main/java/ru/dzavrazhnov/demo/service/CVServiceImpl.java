package ru.dzavrazhnov.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.dzavrazhnov.demo.DTO.CVdto;
import ru.dzavrazhnov.demo.model.CV;
import ru.dzavrazhnov.demo.repository.CVRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CVServiceImpl implements CVService {

    @Autowired
    private CVRepository cvRepository;

    @Override
    public List<CV> findAll() {
        return cvRepository.findAll();
    }

    @Override
    public void deleteById(String id) {
        cvRepository.deleteById(id);
    }

    @Override
    public void create(CVdto cvDto) {
        CV cv = CV.builder().
                fullName(cvDto.getFullName()).
                age(cvDto.getAge()).
                email(cvDto.getEmail()).
                bio(cvDto.getBio()).
                phoneNumber(cvDto.getPhoneNumber())
                .build();
        cvRepository.save(cv);
    }


    @Override
    public Optional<CV> getById(String id) {
        return cvRepository.findById(id);
    }

    @Override
    public void save(CV cv) {
        cvRepository.save(cv);
    }


}
