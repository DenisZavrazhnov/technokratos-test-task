package ru.dzavrazhnov.demo.service;

import ru.dzavrazhnov.demo.DTO.CVdto;
import ru.dzavrazhnov.demo.model.CV;

import java.util.List;
import java.util.Optional;

public interface CVService {
    List<CV> findAll();
    void deleteById(String id);
    void create(CVdto cvDto);
    Optional<CV> getById(String id);
    void save(CV cv);

}
