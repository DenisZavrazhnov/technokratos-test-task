package ru.dzavrazhnov.demo.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.dzavrazhnov.demo.model.CV;



@Repository
public interface CVRepository extends MongoRepository<CV, String> {

}
