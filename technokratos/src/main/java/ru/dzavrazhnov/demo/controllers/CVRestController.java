package ru.dzavrazhnov.demo.controllers;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.dzavrazhnov.demo.DTO.CVdto;
import ru.dzavrazhnov.demo.model.CV;
import ru.dzavrazhnov.demo.service.CVService;

import java.util.List;
import java.util.Optional;


@RestController

public class CVRestController {

    @Autowired
    private CVService service;

    @PostMapping ("/saveCV")
    public void saveCV(@RequestBody CVdto cvDto){
        service.create(cvDto);
    }

    @PostMapping ("/refactorSave")
    public void refactorSave(@RequestBody CV cv){
        Optional<CV> cvFromDB = service.getById(cv.getId());
        BeanUtils.copyProperties(cv,cvFromDB,"id");
        service.save(cv);
    }

    @GetMapping("/getCV")
    public List<CV> getAll(){
        return service.findAll();
    }

    @GetMapping("/get/{id}")
    public Optional<CV> getCV(@PathVariable("id")String id){
        return service.getById(id);
    }

    @PostMapping(value = "/del/{id}")
    public void deleteCV(@PathVariable("id")String id){
        service.deleteById(id);
    }



}
